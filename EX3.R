
titanic.file <- file.choose()
titanic.df <- read.csv(titanic.file,na.strings = "")


#1
fun1 <-function(mydf){
  apply(mydf, 2, function(x) any(is.na(x))) 
  
}
#check
fun1(titanic.df)
fun2 <- function (df)
{
  return (as.vector((df)))
}
#check
fun2(fun1(titanic.df))
#2
check2<-fun2(titanic.df$Age)


fun3 <- function (vec)
{
 vec = ifelse(is.na(vec),
                        ave(vec, FUN = function(x) median(x, na.rm = TRUE)),
                        vec)
 return (vec)

}
check2
fun3(check2)
check2
#3
str(titanic.df)
titanic.df[,2] <- factor(titanic.df[,2], levels=c(0,1),  labels = c( "no", "yes"))
titanic.df[,3] <- factor(titanic.df[,3], levels=c(1,2,3),  labels = c( "class A", "class B", "class C"))
